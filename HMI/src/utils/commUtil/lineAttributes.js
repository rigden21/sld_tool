const configAttr = {
  Standard1: {
    stroke: "#848482",
    'stroke-width': 2
  },
  Standard2: {
    stroke: "#000000",
    'stroke-width': 1.5
  },
  POE: {
    stroke: "#F2D031",
    'stroke-width': 1.5
  },
  RS485: {
    stroke: "#00FF00",
    'stroke-width': 1.5
  },
  Ethernet: {
    stroke: "#20A7DB",
    'stroke-width': 1.5
  },
  AnalogInput: {
    stroke: "#EF750F",
    'stroke-width': 1.5
  },
  DigitalInput: {
    stroke: "#A32CC4",
    'stroke-width': 1.5
  },
  OFC: {
    stroke: "#F74990",
    'stroke-width': 1.5
  },
  DigitalOutputOrControlRelay: {
    stroke: "#9B0000",
    'stroke-width': 1.5
  }
};

export default configAttr;
