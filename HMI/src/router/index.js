import Vue from "vue";
import VueRouter from "vue-router";
import store from '@/store/index'
import Container from "../views/Container.vue";
import SLD from "../views/SLD.vue";
import Login from '../views/Login.vue';

Vue.use(VueRouter);

function loginRedirect(to, from, next) {
  if (store.state && store.state.isLogin) {
    next(true)
  } else {
    next({ name: 'login' })
  }
  next()
}

const routes = [
  {
    path: "/",
    alias: "/login",
    name: "login",
    component: Login,
    beforeEnter: (to, from, next) => {
      if (store.state && !store.state.isLogin) {
        next()
      } else {
        next({ name: 'sld' })
      }
      next()
    }
  },
  {
    path: '',
    component: Container,
    name: 'Container',
    children: [
      {
        path: "/sld",
        alias: "/",
        name: "sld",
        component: SLD,
        beforeEnter: loginRedirect
      }
    ]
  }
];



const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

export default router;
