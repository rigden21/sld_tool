const {
  QueryTypes
} = require('sequelize');
const model = require('../db/model');

const express = require('express')
const router = express.Router()


async function updateSldUpdate(plant_id, email) {
  const sQuery = `
  UPDATE tbl_plant
SET sld_update = '${email}'
WHERE plant_id = ${plant_id};`;

  return await model.query(sQuery, {
    type: QueryTypes.UPDATE,
  }).then(res => {
    return res
  }).catch(err => console.log(err, 'ERROR'))
}

router.post('/', async (req, res, next) => {
  const {
    email
  } = req.body.userData
  const plant_id = req.body.fileName.split('.')[0]
  await updateSldUpdate(plant_id, email)
  next()
})

module.exports = router