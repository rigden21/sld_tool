const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk')
const express = require('express')
const router = express.Router()

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

// GETTING THE JSON FILE FROM S3
router.get('/:diagram', (req, res) => {
  const { diagram } = req.params
  const bucketFileNameMap = {
    sld: 'sldImageCollection',
    communicationDiagram: 'communicationDiagramImageCollection'
  }

  const params = {
    Bucket: 'scada4x-assets',
    Key: `SLD_JSON_UTILS/${bucketFileNameMap[diagram]}.json`
  }

  s3.getObject(params, function (err, data) {
    if (err) {
      res.status(400).json(err.message)
    } else {
      const symbol = data.Body.toString()
      res.status(200).json(JSON.parse(symbol))
    }
  })
})


router.get('/directory/:diagram', (req, res) => {
  const { diagram } = req.params
  const assetFolderMap = {
    sld: 'sld_v1.1/',
    communicationDiagram: 'communicationDiagram_v1.0/'
  }
  const params = {
    Bucket: 'scada4x-assets',
    Prefix: `${assetFolderMap[diagram]}`,
    Delimiter: '/'
  }

  s3.listObjectsV2(params, function(err, data) {
    if (err) console.log(err, err.stack); 
    else  res.status(200).json(data)
  });
})

module.exports = router
