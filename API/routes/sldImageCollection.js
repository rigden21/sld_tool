const { accessKeyId, secretAccessKey, region } = require('../config/db');

const aws = require('aws-sdk');
const express = require('express');
const routes = express.Router();

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

routes.post('/', (req, res, next) => {
  const { diagram, directory, key } = req.body

  const bucketFileNameMap = {
    sld: 'sldImageCollection',
    communicationDiagram: 'communicationDiagramImageCollection'
  }
  const assetFolderMap = {
    sld: 'sld_v1.1',
    communicationDiagram: 'communicationDiagram_v1.0'
  }

  const params = {
    Bucket: 'scada4x-assets',
    Key: `SLD_JSON_UTILS/${bucketFileNameMap[diagram]}.json`
  }

  if(req.file) {
    s3.getObject(params, function (err, data) {
      if (err) {
        res.status(400).json(err.message)
      } else {
        const content = JSON.parse(data.Body.toString())
        content[key] = `https://scada4x-assets.s3.ap-south-1.amazonaws.com/${assetFolderMap[diagram]}/${directory}/${req.file.originalname}`
        const bufferContent = Buffer.from(JSON.stringify(content, null, 4))
        const fileType = 'application/json'
        const params2 = {
          Bucket:  'scada4x-assets',
          Key: `SLD_JSON_UTILS/${bucketFileNameMap[diagram]}.json`,
          Body:  bufferContent,
          ContentType: fileType,
          ACL: 'public-read'
        }

        s3.upload(params2, function(s3Err, result) {
          if(s3Err) {
            console.log(s3Err, 'S3 ERROR')
            // throw s3Err 
            res.status(400).json({message: s3Err})
          }

          next()
        })
      }
    })
  }
})

module.exports = routes;

