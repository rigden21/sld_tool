import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuetify from 'vuetify'
import lodash from 'lodash'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;
Vue.use(lodash)

Vue.use(Vuetify, {
  iconfont: 'mdi',
  theme: {
    primary: '#FF7043',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
