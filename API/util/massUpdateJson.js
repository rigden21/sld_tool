const aws = require('aws-sdk');
const { accessKeyId, secretAccessKey, region } = require('../config/db');
const dotenv = require('dotenv');
dotenv.config()

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

// TO KNOW: tbl_component_config is where they store the parameter map config
function massUpdateJson() {
  // GET THE ALL THE files in the folder
  const updatedJsonList = []

  const params2 = {
    Bucket: 'scada4x-assets',
    Prefix: 'sldAsset/',
    Delimiter: '/'
  }
  s3.listObjectsV2(params2, function(err, listJson) {
    if (err)  throw err
    else {
      listJson.Contents.shift()
      listJson.Contents.forEach(keyItem => {
        // GET THE JSON
        const params = {
          Bucket: 'scada4x-assets',
          Key: keyItem['Key']
        }

        s3.getObject(params, function (err, data) {
          if (err) {
            console.log(err, 'ERROR IN THE CODE')
          } else {
            const content = JSON.parse(data.Body.toString())
            let isUpdateTrue = false
            content['tabs'].forEach(tab => {
              const isScbPresent = content[tab]['devices'].some(el => el == 'scb')
              if (isScbPresent && content[tab]['table']['scb']['paramMap']['DC Power (kW)']) {
                // This code is to keep the index of keys same in altered json
                const paramMapObj = {
                  'Voltage (V)': 'Vdc'
                }
                const thresholdObj = {
                  'Voltage (V)': 0
                }
                Object.keys(content[tab]['table']['scb']['paramMap']).forEach(el => {
                  if (el != 'DC Power (kW)') {
                    paramMapObj[el] = content[tab]['table']['scb']['paramMap'][el]
                    thresholdObj[el] = content[tab]['table']['scb']['threshold'][el]
                  }
                })
                content[tab]['table']['scb']['paramMap'] = paramMapObj
                content[tab]['table']['scb']['threshold'] = thresholdObj
                isUpdateTrue = true
              }
            })

            if (isUpdateTrue) {
              updatedJsonList.push(keyItem['Key'])
              const buff = Buffer.from(JSON.stringify(content, null, 4));
              const uploadingParam = {
                Bucket:  'scada4x-assets',
                Key: `${keyItem['Key']}`,
                Body:  buff,
                ACL: 'public-read',
                ContentType: `application/json`
              }

              s3.upload(uploadingParam, function(s3Err, result) {
                if(s3Err) {
                  throw s3Err 
                } else {
                  console.log(result, 'DONE UPLOADING')
                }
              })
            }
          }
        })
      })
    }
  })
  console.log(updatedJsonList, 'DONE UPLOADING')
}

massUpdateJson()