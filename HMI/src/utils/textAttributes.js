const configAttr = {
  block_headertext: {
    fill: "#FF7043",
    "font-size": "16px",
    "font-weight": "bold",
  },
  block_subHeaderText: {
    fill: "#FF7043",
    "font-size": "12px",
    "font-weight": "bold",
  },
  gridLine_text: {
    fill: "#ADD8E6",
    "font-size": "16px",
    "font-weight": "bold",
  },
  header_text: {
    fill: "#ADD8E6",
    "font-size": "12px",
    "font-weight": "bold",
  },
  block_text: {
    fill: "#ADD8E6",
    "font-size": "16px",
    "font-weight": "bold",
  },
  info_text: {
    fill: "#ADD8E6",
    "font-size": "8px",
  },
  conn_text: {
    fill: "#ADD8E6",
    "font-size": "11px",
    "font-weight": "bold",
  },
  side_text: {
    fill: "#ADD8E6",
    "font-size": "16px",
    "font-weight": "bold",
  },
  text_link: {
    fill: "#ADD8E6",
    "font-size": "16px",
    "font-weight": "bold",
    cursor: "pointer",
  }
}

export default configAttr;