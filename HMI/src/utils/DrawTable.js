function WriteTableRow(col, x, y, width, height, paper, TDdata) {
  const TD = TDdata.split(",");
  const cell = {};
  for (var j = 0; j < TD.length; j++) {
    const rect = paper.rect(x, y, width, height).attr({
      fill: "#1C2137",
      "font-weight": "bold",
      stroke: "#ffffff",
      "stroke-width": "0.25",
    });

    const text = paper.text(x + width / 2, y + height / 2, TD[j]).attr({
      fill: col,
    });
    x = x + width;
    cell["box"] = rect;
    cell["value"] = text;
  }
  return cell;
}

function WriteAnnTableRow(col, x, y, width, height, paper, TDdata) {
  const TD = TDdata.split(",");
  const cell = {};
  for (let j = 0; j < TD.length; j++) {
    const rect = paper.rect(x, y, width, height).attr({
      fill: "#1C2137",
      "font-weight": "bold",
      stroke: "#ffffff",
      "stroke-width": "0.25",
    });

    const cir = paper.circle(x + width / 2, y + height / 2, 5).attr({
      fill: col,
      "stroke-width": "0",
    });

    cell["box"] = rect;
    cell["value"] = cir;
  }
  return cell;
}

export { WriteTableRow, WriteAnnTableRow };
