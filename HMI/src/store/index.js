import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate'
import _ from "lodash";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLogin: false,
    userData: {},
    isProgressBar: false,
    tableParameter: {
      table: {},
      hover: {}
    }
  },
  mutations: {
    SET_IS_LOGIN (state, props) {
      state.isLogin = props;
    },
    SET_USER_DATA (state, props) {
      state.userData = _.cloneDeep(props);
    },
    SET_PROGRESS (state, props) {
      state.isProgressBar = props;
    },
    SET_TABLE_PARAMETER (state, props) {
      for (const [key, value] of Object.entries(props)) {
        Vue.set(state.tableParameter['table'], key, value);
      }
    },
    SET_HOVER_PARAMETER (state, props) {
      for (const [key, value] of Object.entries(props)) {
        Vue.set(state.tableParameter['hover'], key, value);
      }
    }
  },
  actions: {},
  getters: {
    getUserData(state) {
      return state.userData;
    },
    getIsLogin(state) {
      return state.isLogin;
    },
    getIsProgressBar(state) {
      return state.isProgressBar;
    },
    getTableParameter(state) {
      return state.tableParameter
    }
  },
  plugins: [createPersistedState()]
});
