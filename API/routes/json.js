const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk')
const express = require('express')
const router = express.Router()

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

router.get('/', (req, res) => {
  const params1 = {
    Bucket: 'scada4x-assets',
    Prefix: 'sldAsset/',
    Delimiter: '/'
  }

  const params2 = {
    Bucket: 'scada4x-assets',
    Prefix: 'sldDevAsset/',
    Delimiter: '/'
  }

  const param3 = {
    Bucket: 'scada4x-assets',
    Prefix: 'communicationDiagramAsset/',
    Delimiter: '/'
  }
  s3.listObjectsV2(params1, function(err, data1) {
    if (err)  throw err
    else {
      s3.listObjectsV2(params2, function(err, data2) {
        if (err)  throw err
        else {
          s3.listObjectsV2(param3, function(err, data3) {
            if (err)  throw err
            else {
              res.status(200).json({
                production: data1,
                development: data2,
                communication: data3
              })
            }
          })
        }
      })
    }
  })
})

module.exports = router