const http = require('http');
const app = require('./app')
const dotenv = require('dotenv');
dotenv.config()

const PORT = process.env.PORT || 3090 // production
const server = http.createServer(app);
server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`)
})
