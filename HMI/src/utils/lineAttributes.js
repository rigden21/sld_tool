const configAttr = {
  lineStrokeColorDark: {
    stroke: "#ADD8E6",
  },
  lineStrokeColorDisable: {
    stroke: "#9E9E9E",
  },
  lineStrokeColorGreen: {
    stroke: "#3CB371",
  },
  lineStrokeColorLight: {
    stroke: "#000000",
  }
};
export default configAttr;
