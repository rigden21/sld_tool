const configAttr = {
  lineRectStrokeColorPurple: {
    stroke: "#cc99ff",
    "stroke-dasharray": "--",
  },
  dash_rect: {
    stroke: "#ADD8E6",
    "stroke-dasharray": "--",
  },
  dash_disable_rect: {
    stroke: "#9E9E9E",
    "stroke-dasharray": "--",
  },
  dash_disable_rect_orange: {
    stroke: "#ff9900",
    "stroke-dasharray": "--",
  },
  
};
export default configAttr;
