const Sequelize = require('sequelize');
const config = require('../../config/db');

const { database, username, password, host, dialect } = config.sequelize

const createSqlClient = () => {
  const connectionOptions = {
    database,
    dialect,
    timezone: '+05:30',
    logging : true,
    retry   : 5,
    pool    : {
      max: 5,
      min: 0
    },
    ...(
      host && { username, password, host }
    )
  }

  return new Sequelize(connectionOptions)
}

const model = createSqlClient()

module.exports = model