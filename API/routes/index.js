const dotenv = require('dotenv');
dotenv.config()

const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk');

const express = require('express');
const routes = express.Router();

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

// sldAsset/
routes.post('/', (req, res) => {
  const content = JSON.stringify(req.body.content, null, 4)
  const bufferContent = Buffer.from(content)
  const destination = process.env[`${req.body.destination.toUpperCase()}_FOLDER`]
  const objectType = 'application/json'
  const params = {
    Bucket: 'scada4x-assets',
    Key: `${destination}/${req.body.fileName}`,
    Body: bufferContent,
    ACL: 'public-read',
    ContentType: objectType
  }

  s3.upload(params, function (s3Err, result) {
    if (s3Err) {
      throw s3Err
    } else {
      res.status(200).json({
        message: 'success'
      })
    }
  })
})

module.exports = routes;

