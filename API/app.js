const express = require('express');
const path = require('path');
const logger = require('morgan');
const multer = require('multer');
const cors = require('cors');

const indexRouter = require('./routes/index');
const commExport = require('./routes/commExport');
const svgRouter = require('./routes/svg');
const uploadRouter = require('./routes/upload');
const sldImageCollectionEdit = require('./routes/sldImageCollection')
const jsonRouter = require('./routes/json')
const imoprtS3JsonFile = require('./routes/imoprtS3JsonFile')
const updateJsonUser = require('./routes/updateJsonUser')

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'temp');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const whitelist = ['image/svg+xml']
const fileFilter = (req, file, cb) => {
  if (!whitelist.includes(file.mimetype)) {
    return cb(new Error('file is not allowed'))
  }

  cb(null, true)
};

app.use(multer({ storage: fileStorage, fileFilter: fileFilter, limits: { fieldSize: 8 * 1024 * 1024 } }).single(
  'file'
))
app.use('/temp', express.static(path.join(__dirname, 'temp')));


app.use('/sld/exportJson', updateJsonUser, indexRouter);
// updateJsonUser => keeps the record of who made the change in tbl_plant => sld_update column
app.use('/communicationDiagram/exportJson', commExport);
// Import JSON file names
app.use('/fileJson', jsonRouter);
// Getting svg images from s3 bucket
app.use('/svg', svgRouter);
app.use('/uploadSVG', sldImageCollectionEdit, uploadRouter);

// Import JSON config for a plant
app.use('/import/sld/s3File', imoprtS3JsonFile);

app.use('/import/communicationDiagram/s3File', imoprtS3JsonFile);

module.exports = app;
