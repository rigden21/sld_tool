import axios from 'axios'

const defaultApi = () => {
  const Api = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL ? process.env.VUE_APP_BASE_URL : 'http://' + location.hostname + ':3090'
  })

  // Default timeout set to 30s
  Api.defaults.timeout = 30000

  Api.interceptors.request.use(function (request) {
    if (localStorage.getItem('vue-session-key') != null) {
      request.headers.Authorization = 'Bearer ' + JSON.parse(localStorage.getItem('vue-session-key')).jwt
    }
    return request
  }, (error) => {
    return Promise.reject(error)
  })

  Api.interceptors.response.use(function (res) {
    return res
  }, error => {
    return Promise.reject(error)
  })
  return Api
}

export const Api = () => {
  return {
    post: (...params) => {
      return defaultApi().post(...params)
    },
    patch: (...params) => {
      return defaultApi().patch(...params)
    },
    get: (...params) => {
      return defaultApi().get(...params)
    },
    delete: (...params) => {
      return defaultApi().delete(...params)
    },
    request: (params) => {
      return defaultApi().request(Object.assign(params))
    },
    put: (...params) => {
      return defaultApi().put(...params)
    }
  }
}
