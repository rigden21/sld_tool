const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path')

const express = require('express');
const routes = express.Router();

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

routes.post('/', (req, res) => {
  const { diagram, directory } = req.body

  const assetFolderMap = {
    sld: 'sld_v1.1',
    communicationDiagram: 'communicationDiagram_v1.0'
  }

  
  if(req.file) {
    const filePath = path.join(__dirname, '..', 'temp', `${req.file.originalname}`)
      
    fs.readFile(filePath, (err, buff) => {
      if(err) {
        console.log('ERROR', err);
        res.status(400).json({message: err})
      } else {
        const param = {
          Bucket:  'scada4x-assets',
          Key: `${assetFolderMap[diagram]}/${directory}/${req.file.originalname}`,
          Body:  buff,
          ACL: 'public-read',
          ContentType: `${req.file.mimetype}`
        }

        s3.upload(param, function(s3Err, result) {
          if(s3Err) {
            throw s3Err 
          } else {
            // req.body.Location =  result.Location;
            fs.unlink(path.join(__dirname, '..', 'temp', `${req.file.originalname}`), (err) => {
              if(err) {
                throw (err);
              }
              res.status(200).json({ message: 'success' })
            })
          }
        })
      }
    })
  }
})

module.exports = routes;

