const aws = require('aws-sdk');
const express = require('express');
const routes = express.Router();
const { accessKeyId, secretAccessKey, region } = require('../config/db');
const dotenv = require('dotenv');
dotenv.config()

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

routes.get('/:destination/:name', (req, res) => {
  const dest = req.params.destination == 'Development' ? process.env.DEVELOPMENT_FOLDER : process.env.PRODUCTION_FOLDER
  const params = {
    Bucket: 'scada4x-assets',
    Key: `${dest}/${req.params.name}`
  }

  s3.getObject(params, function (err, data) {
    if (err) {
      res.status(400).json(err.message)
    } else {
      const content = JSON.parse(data.Body.toString())
      res.status(200).json(content)
    }
  })
})

routes.get('/:name', (req, res) => {
  const dest = process.env.COMM_FOLDER
  const params = {
    Bucket: 'scada4x-assets',
    Key: `${dest}/${req.params.name}`
  }

  s3.getObject(params, function (err, data) {
    if (err) {
      res.status(400).json(err.message)
    } else {
      const content = JSON.parse(data.Body.toString())
      res.status(200).json(content)
    }
  })
})

module.exports = routes;

